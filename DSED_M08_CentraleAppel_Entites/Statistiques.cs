﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M08_CentraleAppel_Entites
{
    public class Statistiques
    {
        public int NombreAppelsJourneeEnCours { get; private set; }
        public TimeSpan TempsMoyenParAppel { get; private set; }
        public int NombreAgentsPresents { get; private set; }
        public Statistiques(int p_nombreAppelsJourneeEnCours, TimeSpan p_tempsMoyenParAppel, int p_nombreAgentsPresents)
        {
            if(p_nombreAppelsJourneeEnCours < 0)
            {
                throw new ArgumentOutOfRangeException("Le nombre d'appels dans la journée est éronné.");
            }
            if(p_nombreAgentsPresents < 0)
            {
                throw new ArgumentOutOfRangeException("Le nombre d'agents présents est éronné.");
            }
            if(p_tempsMoyenParAppel < TimeSpan.MinValue)
            {
                throw new ArgumentOutOfRangeException("Le temps moyen par appel est éronné.");
            }

            this.NombreAppelsJourneeEnCours = p_nombreAppelsJourneeEnCours;
            this.TempsMoyenParAppel = p_tempsMoyenParAppel;
            this.NombreAgentsPresents = p_nombreAgentsPresents;
        }
    }
}
