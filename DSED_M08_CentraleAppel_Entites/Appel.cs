﻿namespace DSED_M08_CentraleAppel_Entites
{
    public class Appel
    {
        public DateTime Debut { get; private set; }
        public DateTime Fin { get; private set; }
        public int Agent { get; private set; }
        public Appel(int p_agent)
        {
            if(p_agent < 1)
            {
                throw new ArgumentOutOfRangeException("L'identifiant de l'agent est invalide.");
            }

            this.Debut = DateTime.Now;
            this.Agent = p_agent;
        }
        public Appel(int p_agent, DateTime p_debut): this (p_agent)
        {
            if(p_debut == DateTime.MinValue)
            {
                throw new ArgumentOutOfRangeException("La date de début d'appel est invalide.");
            }

            this.Debut = p_debut;
        }
        public Appel(int p_agent, DateTime p_debut, DateTime p_fin) : this(p_agent, p_debut)
        {
            this.Fin = p_fin;
        }
        public void TerminerAppel()
        {
            if(this.Fin != DateTime.MinValue)
            {
                throw new InvalidOperationException("L'appel a déjà est notifié comme terminé.");
            }

            this.Fin = DateTime.Now;
        }
    }
}