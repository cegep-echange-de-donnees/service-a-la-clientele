﻿using Microsoft.AspNetCore.Mvc;
using DSED_M08_CentraleAppel_API.Models;
using DSED_M08_CentraleAppel_API.Hubs;
using DSED_M08_CentraleAppel_Entites;
using Microsoft.AspNetCore.SignalR;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DSED_M08_CentraleAppel_API.Controllers
{
    [Route("api/appels")]
    [ApiController]
    public class AppelController : ControllerBase
    {
        private readonly IHubContext<CentraleAppelsHub> m_hubContext;
        private IDepotAppels m_depotAppels;
        public AppelController(IDepotAppels p_depotAppels, IHubContext<CentraleAppelsHub> p_hubContext)
        {
            if(p_hubContext == null)
            {
                throw new ArgumentNullException(nameof(p_hubContext));
            }
            if(p_depotAppels == null)
            {
                throw new ArgumentNullException(nameof(p_depotAppels));
            }

            this.m_hubContext = p_hubContext;
            this.m_depotAppels = p_depotAppels;
        }
        // GET: api/appels
        [HttpGet]
        [ProducesResponseType(403)]
        public ActionResult<List<Models.Appel>> Get()
        {
            return StatusCode(403);
        }

        // GET api/appels/{agent}
        [HttpGet("{agent}")]
        [ProducesResponseType(403)]
        public ActionResult<Models.Appel> Get(int agent)
        {
            return StatusCode(403);
        }

        // POST api/appels
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult> Post([FromBody] Models.Appel p_appel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                DSED_M08_CentraleAppel_Entites.Appel appel = new DSED_M08_CentraleAppel_Entites.Appel(p_appel.Agent);

                this.m_depotAppels.CreerAppel(appel);
                p_appel.Debut = appel.Debut;
                p_appel.Fin = DateTime.MinValue;

                DSED_M08_CentraleAppel_Entites.Statistiques statistiques = this.m_depotAppels.ObtenirStatistiques();
                Models.Statistiques statistiquesModel = new Models.Statistiques();
                statistiquesModel.NombreAppelsJourneeEnCours = statistiques.NombreAppelsJourneeEnCours;
                statistiquesModel.TempsMoyenParAppel = statistiques.TempsMoyenParAppel;
                statistiquesModel.NombreAgentsPresents = statistiques.NombreAgentsPresents;

                await m_hubContext.Clients.All.SendAsync("MettreAJourStatistiques", statistiquesModel);

                return CreatedAtAction(nameof(Get), new { id = appel.Agent }, p_appel);
            }
            catch(ArgumentOutOfRangeException)
            {
                return BadRequest();
            }
            catch(ArgumentNullException)
            {
                return BadRequest();
            }
        }

        // PUT api/appels/{agent}
        [HttpPut("{agent}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult> Put(int agent, [FromBody] Models.Appel p_appel)
        {
            if (!ModelState.IsValid || p_appel.Agent != agent)
            {
                return BadRequest();
            }

            try
            {
                DSED_M08_CentraleAppel_Entites.Appel appel = new DSED_M08_CentraleAppel_Entites.Appel(p_appel.Agent, p_appel.Debut);
                this.m_depotAppels.TerminerAppel(appel);
                p_appel.Fin = appel.Fin;

                DSED_M08_CentraleAppel_Entites.Statistiques statistiques = this.m_depotAppels.ObtenirStatistiques();
                Models.Statistiques statistiquesModel = new Models.Statistiques();
                statistiquesModel.NombreAppelsJourneeEnCours = statistiques.NombreAppelsJourneeEnCours;
                statistiquesModel.TempsMoyenParAppel = statistiques.TempsMoyenParAppel;
                statistiquesModel.NombreAgentsPresents = statistiques.NombreAgentsPresents;

                await m_hubContext.Clients.All.SendAsync("MettreAJourStatistiques", statistiquesModel);
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest();
            }
            catch(ArgumentNullException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/appels/{agent}
        [HttpDelete("{agent}")]
        [ProducesResponseType(403)]
        public ActionResult Delete(int agent)
        {
            return StatusCode(403);
        }
    }
}
