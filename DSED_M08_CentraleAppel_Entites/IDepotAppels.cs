﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M08_CentraleAppel_Entites
{
    public interface IDepotAppels
    {
        public void CreerAppel(Appel p_appel);
        public void TerminerAppel(Appel p_appel);
        public int ObtenirNombreAppelsJourneeEnCours();
        public TimeSpan ObtenirTempsMoyenParAppel();
        public int ObtenirNombreAgentsPresents();
        public Statistiques ObtenirStatistiques();
    }
}
