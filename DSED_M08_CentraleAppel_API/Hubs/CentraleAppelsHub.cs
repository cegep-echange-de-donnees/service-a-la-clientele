﻿using Microsoft.AspNetCore.SignalR;
using DSED_M08_CentraleAppel_API.Models;
using DSED_M08_CentraleAppel_Entites;

namespace DSED_M08_CentraleAppel_API.Hubs
{
    public class CentraleAppelsHub : Hub
    {
        private IDepotAppels m_depotAppels;
        public CentraleAppelsHub(IDepotAppels p_depotAppels)
        {
            if(p_depotAppels == null)
            {
                throw new ArgumentNullException(nameof(p_depotAppels));
            }

            this.m_depotAppels = p_depotAppels;
        }
        public Models.Statistiques ObtenirStatistiques()
        {
            DSED_M08_CentraleAppel_Entites.Statistiques statistiques = this.m_depotAppels.ObtenirStatistiques();
            Models.Statistiques statistiquesModel = new Models.Statistiques();
            statistiquesModel.NombreAppelsJourneeEnCours = statistiques.NombreAppelsJourneeEnCours;
            statistiquesModel.TempsMoyenParAppel = statistiques.TempsMoyenParAppel;
            statistiquesModel.NombreAgentsPresents = statistiques.NombreAgentsPresents;

            return statistiquesModel;
        }
        public async override Task OnConnectedAsync()
        {
            Models.Statistiques statistiques = this.ObtenirStatistiques();
            await Clients.Caller.SendAsync("MettreAJourStatistiques", statistiques);
            await base.OnConnectedAsync();
        }
    }
}
