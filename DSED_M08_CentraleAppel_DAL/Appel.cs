﻿using DSED_M08_CentraleAppel_Entites;

namespace DSED_M08_CentraleAppel_DAL
{
    public class Appel
    {
        public DateTime Debut { get; set; }
        public DateTime Fin { get; set; }
        public int Agent { get; set; }
        public Appel()
        {
            ;
        }
        public Appel(DSED_M08_CentraleAppel_Entites.Appel p_appel)
        {
            if(p_appel == null)
            {
                throw new ArgumentNullException(nameof(p_appel));
            }
            this.Debut = p_appel.Debut;
            this.Fin = p_appel.Fin;
            this.Agent = p_appel.Agent;
        }
        public DSED_M08_CentraleAppel_Entites.Appel VersEntite()
        {
            return new DSED_M08_CentraleAppel_Entites.Appel(this.Agent, this.Debut, this.Fin);
        }
    }
}