using DSED_M08_CentraleAppel_API.Hubs;
using Microsoft.AspNetCore.SignalR;
using DSED_M08_CentraleAppel_Entites;
using DSED_M08_CentraleAppel_DAL;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddSignalR();
builder.Services.AddSwaggerDocument();
builder.Services.AddSingleton<IDepotAppels, DepotAppels>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapHub<CentraleAppelsHub>("/CentraleAppelsHub");
    endpoints.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
});

app.UseOpenApi();
app.UseSwaggerUi3();

app.Run();
