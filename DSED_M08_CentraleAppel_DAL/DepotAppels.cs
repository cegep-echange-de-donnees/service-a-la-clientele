﻿using DSED_M08_CentraleAppel_Entites;

namespace DSED_M08_CentraleAppel_DAL
{
    public class DepotAppels : IDepotAppels
    {
        private List<Appel> m_listeAppels = new List<Appel>()
        {
            new Appel(){Agent = 1, Debut = DateTime.Now, Fin = DateTime.Now.AddMinutes(6)},
            new Appel(){Agent = 2, Debut = DateTime.Now, Fin = DateTime.Now.AddMinutes(12)},
            new Appel(){Agent = 2, Debut = DateTime.Now.AddMinutes(12), Fin = DateTime.Now.AddMinutes(17)},
            new Appel(){Agent = 3, Debut = DateTime.Now, Fin = DateTime.Now.AddMinutes(2)},
            new Appel(){Agent = 3, Debut = DateTime.Now.AddMinutes(2), Fin = DateTime.Now.AddMinutes(11)}
        };

        public DepotAppels()
        {
            ;
        }
        public void CreerAppel(DSED_M08_CentraleAppel_Entites.Appel p_appel)
        {
            if(p_appel == null)
            {
                throw new ArgumentNullException("L'appel est invalide.");
            }

            this.m_listeAppels.Add(new Appel(p_appel));
        }
        public void TerminerAppel(DSED_M08_CentraleAppel_Entites.Appel p_appel)
        {
            if (p_appel == null)
            {
                throw new ArgumentNullException("L'appel n'est pas valide");
            }

            Appel? appelATermine = this.m_listeAppels.Where(appel => appel.Agent == p_appel.Agent
                                                            && appel.Debut == p_appel.Debut
                                                            && appel.Fin == DateTime.MinValue)
                                                     .SingleOrDefault();
            if (appelATermine == null)
            {
                throw new ArgumentNullException("L'appel n'existe pas ou est déjà terminé");
            }

            DSED_M08_CentraleAppel_Entites.Appel appelEntite = appelATermine.VersEntite();
            appelEntite.TerminerAppel();
            p_appel.TerminerAppel();

            Appel appelMisAJour = new Appel(appelEntite);

            this.m_listeAppels.Remove(appelATermine);
            this.m_listeAppels.Add(appelMisAJour);
        }
        public int ObtenirNombreAgentsPresents()
        {
            return this.m_listeAppels.Where(appel => appel.Debut.Date == DateTime.Today)
                                     .GroupBy(appel => appel.Agent)
                                     .Count();
        }
        public int ObtenirNombreAppelsJourneeEnCours()
        {
            return this.m_listeAppels.Where(appel => appel.Debut.Date == DateTime.Today)
                                     .Count();
        }
        public TimeSpan ObtenirTempsMoyenParAppel()
        {
            List<TimeSpan> tempsAppels = new List<TimeSpan>();
            tempsAppels = this.m_listeAppels.Where(appel => appel.Fin != DateTime.MinValue)
                                            .Select(appel => appel.Fin - appel.Debut)
                                            .ToList();

            TimeSpan tempsMoyen = new TimeSpan();
            tempsAppels.ForEach(temps => tempsMoyen += temps.Divide(Convert.ToDouble(tempsAppels.Count())));

            return tempsMoyen;
        }
        public Statistiques ObtenirStatistiques()
        {
            int nombreAppelsJourneeEnCours = this.ObtenirNombreAppelsJourneeEnCours();
            TimeSpan tempsMoyenParAppel = this.ObtenirTempsMoyenParAppel();
            int nombreAgentsPresents = this.ObtenirNombreAgentsPresents();

            return new Statistiques(nombreAppelsJourneeEnCours, tempsMoyenParAppel, nombreAgentsPresents);
        }
    }
}
