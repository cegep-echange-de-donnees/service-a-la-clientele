﻿namespace DSED_M08_CentraleAppel_API.Models
{
    public class Appel
    {
        public DateTime Debut { get; set; }
        public DateTime Fin { get; set; }
        public int Agent { get; set; }
        public Appel()
        {
            ;
        }
    }
}
