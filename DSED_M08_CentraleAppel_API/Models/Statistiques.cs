﻿namespace DSED_M08_CentraleAppel_API.Models
{
    public class Statistiques
    {
        public int NombreAppelsJourneeEnCours { get; set; }
        public TimeSpan TempsMoyenParAppel { get; set; }
        public int NombreAgentsPresents { get; set; }
        public Statistiques()
        {
            ;
        }
    }
}
